package com.socialNetwork.networkV2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetworkV2Application {

	public static void main(String[] args) {
		SpringApplication.run(NetworkV2Application.class, args);
	}

}
